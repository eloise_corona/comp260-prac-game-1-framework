﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	public Vector2 move;
    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 8.0f; // in metres/second/second
    public float brake = 20.0f; // in metres/second/second
    private float speed = 0.0f;    // in metres/second
    public float turnSpeed = 30.0f; // in degrees/second
	private float currentturnSpeed;
	public string H, V;

    void Update()
    {
		Debug.Log ("H Value: " + Input.GetAxis(H) + " | V Value: " + Input.GetAxis(V));
        // the horizontal axis controls the turn
        float turn = Input.GetAxis(H);
		currentturnSpeed = turnSpeed - turnSpeed * Mathf.Clamp (speed / maxSpeed, 0, 0.6f);

        // turn the car
        transform.Rotate(0, 0, -turn * turnSpeed * Time.deltaTime);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(V);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
				speed = Mathf.Max(speed - brake * Time.deltaTime,0);


            }
			else if (speed < 0)
            {
				speed = Mathf.Min(speed + brake * Time.deltaTime,0);
            }
        }


        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;

        // move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
    }


    // Use this for initialization
    void Start () {
		
	}
}
